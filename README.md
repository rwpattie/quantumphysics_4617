# QuantumPhysics_4617

Scripts for the PHYS-4617 course at ETSU.  Spring 2020.
Author: R.W. Pattie Jr
email : pattie@etsu.edu


The scripts were written to aid in visualizing certain quantum systems,
initially superpositions of the 1-D Harmonic Oscillator. I'll continue adding
scripts as I continue to teach this course.

Content:
------------
harmonic_oscillator: 
	This python3 based script will generate the wavefunctions for the 1D
        oscillator by summing the eigenenergy states.  While there should be
        no upper limit for this, I've found python3 (or my laptop) fails around
        order n=25 to compute the factorial in the normalization. The main
        purpose is time evolve these wavefunctions and observe the behavior
        of the probability density and the expectation value of the position,
        P(x,t) and <x(t)>.
        I use the ffmeg command line tool to generate a mp4 from the png's 
        created by the script.   
