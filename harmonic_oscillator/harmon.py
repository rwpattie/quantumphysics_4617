import numpy as np
import matplotlib.pyplot as plt
import sys
import getopt
from numpy.polynomial.hermite import hermval

# --------------------------------------------------------------------------------
#
# The following is a script for calculating the wavefunction, probability density
# and position expectation value for the harmonic oscillator in 1D. This was
# written as a demonstration tool for Quantum Physics PHYS-4617 at ETSU.
#
# Author : R. W. Pattie Jr.
# Date   : 3/18/2020
# Affiliation : East Tennessee State University Department of Physics Astronomy
# email  : pattie@etsu.edu
#
#
# usage : python3 harmon.py -d <int> -c <int,int,int,...> -w <float> -m <float> -t <int>
#
#         d : Number of time divisions per tmax=2/w.  The calculation will
#             perform 5xd calculations to cover a period of time equalt to 10/w
#
#         w : omega, the frequency of oscillation.  This is a stand in for the
#             "spring constant" of the system. w=sqrt(k/m).  This doesn't really
#             need to be altered.
#
#         m : mass of the particle in units of eV/c^2
#
#         t : number of tmax cycles to calculate default is 5
#
# The command line tool ffmpeg can be used to stitch the generated .png files into a movie
#
#  ffmpeg -framerate 15 -i test_%03d.png -c:v libx264 -r 30 -pix_fmt yuv420p vid_c00001_xexp.mp4
#
#
#-------------------------------------------------------------------------------
def byhand(x,omega):
    # --------------------------------------------------------------------------
    # This method calculates the wavefunction for the state:
    #  psi(x) = phi_0(x) + phi_1(x)
    #
    # The purpose is to verify that the python library Hermite Polynomial
    # series produces what I think it should.
    #
    # Spoiler, it seems to work.
    #
    # --------------------------------------------------------------------------  
    m    = 511.0e3/np.power(2.99e8,2) # electron mass
    hbar = 6.582119569e-16 # hbar in units of eV seconds
    # define the variable of the Hermite series
    xi   = np.sqrt((m*omega)/hbar)*x
    prob = 0+0j
    prob = np.power(m*omega/(hbar*np.pi),1./4.)*np.exp(-m*omega*x*x/(2.*hbar))
    #
    # Coefficients for the 0 and 1 energy states
    #
    c0 =  1./np.sqrt(np.power(2,0)*np.math.factorial(0))
    c1 =  1./np.sqrt(np.power(2,1)*np.math.factorial(1))
    # Sum the polynomials
    Hsum = c0 + c1*2.*xi
    #
    prob = prob*Hsum
    
    return prob
#-------------------------------------------------------------------------------    
def phi0(x,omega,coefs,t):
    #calculating the hermite polynomial expansion
    m    = 511.0e3 # electron mass
    hbar = 6.582119569e-16 # hbar in units of eV seconds
    prob = 0+0j
    # define the variable of the Hermite series
    xi   = np.sqrt((m*omega)/hbar)*x
    
    for ii in range(0,len(coefs)):
        # Normalization from the raising operators
        co =  1./np.sqrt(np.power(2,ii)*np.math.factorial(ii))
        # Dumb hack to get the build an order n array of coefficients 
        cff = []
        # create empty array up to the length of the iith state
        for i in range(0,ii+1):
            cff.append(0)
        # set the coefficient of the last state to be the what was passed
        cff[-1] = coefs[ii] 
        # add this to the wavefunction
        prob = np.complex(prob,0) + np.complex( co*hermval(xi,cff),0)*np.exp(-1j*omega*t*(ii+1./2.))

    #set the initial probability for the ground state
    prob = prob*np.power(m*omega/(hbar*np.pi),1./4.)*np.exp(-m*omega*x*x/(2.*hbar))
   
    return prob
#---------------------------------------------------------------------------------
def getwavefunction(cf):
    # this method generates the "name" of the wavefunction being used for the
    # plot title
    norm = 0
    for c in cf:
        norm = norm + c*c
    #start with the normalization and some constants
    wfunc = "$\psi(x,t) =(\\frac{m\omega}{\pi \hbar})^{1/4} \\frac{1}{\sqrt{%2.1f}} \\left["%norm
    ite = 0
    # below 4 terms the expansion fits on the plot
    if len(cf) < 5:
        for c in cf:
            pre = (c/(np.sqrt(np.power(2,ite))*np.math.factorial(ite)))
            if (c != 0):
                wfunc = wfunc + "%2.1f H_{%d}(\\xi) e^{-i %2.1f \omega t}"%(pre,ite,(float(ite)+1./2.))
            ite = ite+1
            if ite < len(cf) and (c != 0):
                wfunc = wfunc + " + "
    else:
        # above 5 terms the expansion is reduced to a sum that shows the max energy level.
        wfunc = wfunc + " \sum_{n=0}^{%d} \\frac{c_n H_{n}(\\xi)}{\sqrt{2^{n} n!}}e^{-iE_{n}t/\hbar} "%(len(cf))
    # tack on the closing bracket and spatial exponent   
    wfunc = wfunc + "\\right] e^{-\\xi^2/2}$ "
    
    return wfunc
#---------------------------------------------------------------------------------
def getvals(xx,ts,w,coef):
    y      = []
    yimg   = []
    yprob  = []
    yhand  = []
    
    for i in xx:
        #print("at x : ",i/xf," and time: ",tt)
        val = phi0(i,w,coef,ts)
        #print("val : " , val)
        y.append(np.real(val))
        yimg.append(np.imag(val))
        yprob.append(val*val.conjugate())
        yhand.append(byhand(i,w))
    #-------------------------------------------------------------------
    # nake sure the distributions are properly normalized
    #--------------------------------------------------------------------
    norm   = 0
    normi  = 0
    normp  = 0
    normby = 0
    xpos1  = 0
    xposd  = 0

    for xxx,yy,pp,bb,yi in zip(xx,y,yprob,yhand,yimg):
        metric = (yy + yi)*(yy + yi.conjugate())
        norm   = norm   + np.sqrt(metric)
        normp  = normp  + pp
        normby = normby + np.abs(bb)
        xpos1  = xpos1  + xxx*pp
        xposd  = xposd  + pp
        
    y     = y     / norm
    yimg  = yimg  / norm
    yprob = yprob / normp
    yhand = yhand / normby
    xexp  = xpos1 / xposd

    return y,yimg,yprob,yhand,xexp
    
#---------------------------------------------------------------------------------
def main(argv):
    
    coef   = [1,1,1] # default coeficients 
    w      = 1.97e8  # oscillation frequency
    m      = 511e3   # electron mass
    hbar   = 6.582119569e-16 # hbar in eV seconds
    xf     = np.sqrt(hbar/(m*w)) # classical turning point
    tmax   = 2./(w)
    ndiv   = 1 # number of time divisions
    ntime  = 5 # number of tmax cycles to perform

    # parse the command line flags
    try:
        opts,args = getopt.getopt(argv,"c:w:m:t:d:",["coef=","energy=","mass=","time=","divisions="])
    except getopt.GetoptError:
        print('harmon.py -c<comma separated coefficients> -w <energy in eV> -m < mass in eV/c^2>')
        sys.exit(2)

    for opt,arg in opts:
        if opt == '-c':
            clist = arg.split(",")
            coef = []
            for i in clist:
                coef.append(float(i))
        elif opt in ('-w','energy='):
            w = float(arg)
        elif opt in ('-m','mass='):
            m = float(arg)
        elif opt in ('-d','divisions='):
            ndiv = int(arg)
        elif opt in ('-t','time='):
            ntime = int(arg)
    #------------------------------------------------------------------------------------------------
    # start the calculation
    # array of positions
    x = np.arange(-5*xf,5*xf,xf*0.001)
    
    tstep = tmax/100 * ndiv 
    print("time step : %5.4e"%tstep)
    # ------------------------------------------------------------
    # evaluate wave function
    itera      = 0  # interator variable 
    timesteps  = [] # array to hold the time stamps
    pos_expect = [] # array to hold the position expection values
    for iit in np.arange(0,tmax*ntime,tmax/ndiv):
        # close the old plot 
        plt.close()
        # if this is the second time throug the loop or great put the
        # position back in terms of absolute distance instead of xf
        if len(timesteps) > 0:
            x = x*xf
        #evaluate the wavefunction at time = iit
        y,yimg,yprob,yhand,xexp = getvals(x,iit,w,coef)
        print("Expectation value of the position is <x> = ",xexp/xf )
        # fill the timestamp and position arrays
        timesteps.append(iit)
        pos_expect.append(xexp/xf)
        #--------------------------------------------------------------
        # put position variable in terms of the classical turning point
        x = x/xf
        #
        # Plot the real and imaginary parts of the wavefunction and the Probability
        # density.
        #
        f, (a0,a1) = plt.subplots(2,1,gridspec_kw={'height_ratios':[3,1]},figsize=[10,8])
        f.tight_layout(pad=2.85)
        # main plot shows the wavefunction parts
        a0.plot(x,y*1000,'-b',label=r'$Re[\psi(x,t)]$')
        a0.plot(x,yimg*1000,'-g',label=r'$Im[\psi(x,t)]$')
        a0.plot(x,yprob*1000,'-r',label=r'$\mathcal{P}(x,t)=|\psi(x,t)|^2$')
        a0.set_ylim(-5e-4*1000,10e-4*1000)
        a0.set_title(r"%s"%getwavefunction(coef),pad=11,fontsize=10)
        #plt.plot(x,yhand,'--g',label='Hand Calc')
        a0.axvline(xexp/xf,ymin=0,ymax=1,color='black',linestyle='--')
        # 
        #ax = plt.axes()
        a0.grid()
        #
        a0.set_xlabel(r'$x_f=\sqrt{\hbar/m\omega}$')
        a0.set_ylabel(r'$\psi(x,t)$ and $|\mathcal{P}(x,t)|^2$ [arb. units]')
        a0.legend(loc=1,title=r'$t=%5.3f\; ns$'%(iit*1e9))
        # secondary plot shows the 
        tstarry = np.asarray(timesteps)
        a1.plot(tstarry*1e9,pos_expect,'--')
        a1.set_xlabel('Time [ns]')
        a1.set_ylabel(r'$\langle x \rangle / x_f$')
        a1.set_xlim(0,5*tmax*1.1e9)
        dis = (len(coef)+1.)/2.
        a1.set_ylim(-dis,dis)
        a1.grid()
        props = dict(boxstyle='round',facecolor='blue',alpha=0.15)
        xstr = r'$\frac{\langle x (t)\rangle}{x_f}$ = %5.4f '%(xexp/xf)
        #
        a0.text(-4.75,0.0008*1000,xstr,fontsize=16,bbox=props)
        plt.savefig("figs/test_%03d.png" % (len(timesteps)))
        itera = itera + 1

    plt.show()
        
if __name__=="__main__":
    main(sys.argv[1:])
